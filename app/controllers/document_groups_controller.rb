class DocumentGroupsController < ApplicationController
  before_action :set_document_group, only: [:show, :edit, :update, :destroy]

  def index
    @document_groups = DocumentGroup.all
  end

  def tree
    @document_groups = DocumentGroup.all
  end

  def show
  end

  def new
    @document_group = DocumentGroup.new
  end

  def edit
  end

  def create
    @document_group = DocumentGroup.new(document_group_params)

    respond_to do |format|
      if @document_group.save
        format.html { redirect_to @document_group, notice: 'Document group was successfully created.' }
        format.json { render action: 'show', status: :created, location: @document_group }
      else
        format.html { render action: 'new' }
        format.json { render json: @document_group.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @document_group.update(document_group_params)
        format.html { redirect_to @document_group, notice: 'Document group was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @document_group.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @document_group.destroy
    respond_to do |format|
      format.html { redirect_to document_groups_url }
      format.json { head :no_content }
    end
  end

  private
    def set_document_group
      @document_group = DocumentGroup.find(params[:id])
    end

    def document_group_params
      params.require(:document_group).permit(:name)
    end
end
