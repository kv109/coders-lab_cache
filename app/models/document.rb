require 'digest/md5'

class Document < ActiveRecord::Base
  belongs_to :document_group, touch: true
  has_and_belongs_to_many :readers

  def checksum
    sleep 2
    Digest::MD5.hexdigest(self.title + cache_key)
  end
end
