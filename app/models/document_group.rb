class DocumentGroup < ActiveRecord::Base
  has_many :documents
  has_many :readers, through: :documents
end
