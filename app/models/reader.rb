class Reader < ActiveRecord::Base
  has_and_belongs_to_many :documents#, touch: true

  def checksum
    sleep 0.2
    Digest::MD5.hexdigest(self.name + cache_key)
  end
end
