class CreateDocumentsReaders < ActiveRecord::Migration
  def change
    create_table :documents_readers, id: false do |t|
      t.references :document
      t.references :reader
    end
  end
end
