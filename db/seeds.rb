# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

4.times do |n|
  DocumentGroup.create! name: "Group no. #{n+1}"
end

10.times do |n|
  puts "Downloading content for document no. #{n+1}"
  Document.create! title: "Document no. #{n+1}", content: JSON.parse(Net::HTTP.get(URI('https://baconipsum.com/api/?type=all-meat&paras=3&start-with-lorem=1'))).join
  puts "Content for document no. #{n+1} downloaded and saved."
end
#
DocumentGroup.all.each do |dg|
  rand(20).times do
    documents = dg.documents
    document = Document.all.sample
    dg.documents << document unless documents.include?(document)
  end
end

8.times do |n|
  reader = Reader.new name: "Reader no. #{n}"
  rand(15).times do
    document = Document.all.sample
    reader.documents << document unless reader.documents.include?(document)
  end
  reader.save!
end
