require 'test_helper'

class ErrorsMessagesControllerTest < ActionController::TestCase
  test "should get error400" do
    get :error400
    assert_response :success
  end

  test "should get error401" do
    get :error401
    assert_response :success
  end

  test "should get error402" do
    get :error402
    assert_response :success
  end

end
